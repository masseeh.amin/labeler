const fs = require('fs')
const path = require('path')

function changeName() {
    //get the file name
    let filePath = this.files[0].path
    //replace the "Choose a file" label
    $(this).next('.custom-file-label').html(filePath)
}

function save(filePath, data, currPic) {
    data['currIdx'] = currPic
    fs.writeFile(filePath, JSON.stringify(data), (err) => {
        if (err) throw err;
        console.log('The file has been saved!');
    });
}

function walkSync(dir, regx, filelist) {
    files = fs.readdirSync(dir)
    filelist = filelist || []
    files.forEach(function(file) {
        if (fs.statSync(path.join(dir, file)).isDirectory()) {
            filelist = walkSync(path.join(dir, file), regx, filelist)
        }
        else {
            if(path.extname(file) === '.jpg' || '.png') {
                if(regx) {
                    if(path.basename(file, path.extname(file)) === regx)
                        filelist.push(path.join(dir, file))
                }
                else {
                    filelist.push(path.join(dir, file))
                }
            }
        }
    });
    return filelist
}

function init(data) {

    let outputDir = data['output']
    let classes = data['classes']
    let datasetPath = data['datasetPath']
    let currPic = data['currIdx'] || 0
    let defultConfig = 'config.json'
    let regx = data["regx"] || null

    let pics = walkSync(datasetPath, regx, [])

    let collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});
    pics.sort(collator.compare)

    $('#myapp').html(
        '<div class="row">' +
            '<div class="col-8" id="pics">' +
            '</div>' +
            '<div class="col-4">' +
                '<div class="card">' +
                    '<div class="card-body">' +
                        '<h5 class="card-title">Data : <span class="badge badge-info" id="track">' + (currPic+1) + '/' + pics.length + '</span></h5>' +
                        '<h5 class="card-title" id="currImg">Current image : ' + pics[currPic] + '</h5>' +
                        '<h5 class="card-title">Output : ' + data["output"] + '</h5>' +
                        '<a href="#" class="btn btn-info" id="save">Save</a>' +
                        ' <a href="#" class="btn btn-danger" id="next">Next</a>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>' +
        '<div class="row mt-4">' +
            '<div class="col" id="classes-choice">' +
            '</div>' +
        '</div>'
    )
    
    $('#pics').html('<img src=' + pics[currPic] + ' class="img-fluid" alt="Responsive image"></img>')

    for(let c in classes) {
        $('#classes-choice').append('<button type="button" class="btn btn-success">' + classes[c] + '</button> ')
    }

    $('#save').click(() => save(defultConfig, data, currPic))

    $('#next').click(() => {
        currPic = currPic + 1
        if(currPic < pics.length) {
            $('#track').text(currPic + "/" + pics.length)  
            $('#currImg').text("Current image : " + pics[currPic])    
            $('#pics').html('<img src=' + pics[currPic] + ' class="img-fluid" alt="Responsive image"></img>')
        }
    })

    $('#classes-choice button').click((event) => {
        let cls = $(event.target).text()
        let clsPath = path.join(outputDir, cls).trim()
        let fileName = null

        if(regx)
            fileName = path.join(clsPath, currPic + '.jpg')
        else
            fileName = path.join(clsPath, pics[currPic].split(path.sep).slice(-1)[0]).trim() 

        let source = fs.createReadStream(pics[currPic])
        source.pipe(fs.createWriteStream(fileName))
        source.on('end', () => { 
            save(defultConfig, data, currPic)
            console.log('Succesfully copied')  
            currPic = currPic + 1
            if(currPic < pics.length) {
                $('#track').text(currPic + "/" + pics.length)  
                $('#currImg').text("Current image : " + pics[currPic])    
                $('#pics').html('<img src=' + pics[currPic] + ' class="img-fluid" alt="Responsive image"></img>')
            }

        });
        source.on('error', (err) =>  console.log(err) );
    })
}

$(() => {

    $('#output').on('change', changeName)
    $('#classes').on('change', changeName)
    $('#dataset').on('change', changeName)
    $('#config').on('change', changeName)

    $('#load').click(() => {
        let configFile = $('#config')[0].files[0].path

        if (!fs.existsSync(configFile)){
            //error
        }

        let data = JSON.parse(fs.readFileSync(configFile, 'utf8'));

        if (!fs.existsSync(data['output'])){
            
            //error
        }
        if (!fs.existsSync(data['datasetPath'])){
            //error
        }

        init(data)
    })


    $('#ok').click(() => {
        //sanity check
        let outputFolder = $('#output')[0].files[0].path
        let classesFile = $('#classes')[0].files[0].path
        let datasetPath = $('#dataset')[0].files[0].path
        let regx = $('#regx').val() || null

        let dir = 'datasets'

        data = {
            "classes" : null,
            "output" : null,
            "datasetPath": null,
            "regx": regx
        }

        if (!fs.existsSync(outputFolder)){
            
            //error
        }
        else {
            dir = path.join(outputFolder, dir)
            if(!fs.existsSync(dir)) fs.mkdirSync(dir)
            data['output'] = dir
        }

        if (!fs.existsSync(classesFile)){
            
            //error
        }
        else {
            data['classes'] = fs.readFileSync(classesFile, 'utf8').split('\n')
        }

        if (!fs.existsSync(datasetPath)){
            //error
        }

        data['datasetPath'] = datasetPath


        let classes = data['classes'] 

        for(let c in classes) {
            let subDir = path.join(dir, classes[c].trim())
            if (!fs.existsSync(subDir)){
                fs.mkdirSync(subDir);
            }
        }

        init(data)
    })
})