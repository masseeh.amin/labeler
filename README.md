# Labeler

The labeler is an electron based application for labeling images for tasks such as classification.
## Getting started

- Install [Node LTS](https://nodejs.org)
- Clone this repository
- `npm install` to install the application's dependencies
- `npm start` to start the application

## How to work with Labeler
1. Select root of your dataset folder
2. Select the output folder (# "datasets" folder automatically will be created in the selected folder)
3. Select the file containing all of the classes names in each line
4. If you want to filter out some specific files you can use regx input
#### Hint:
Once you started the application for the first time, in the root of the application, "config.json" file will be created. You can resume the labeling task by loading the "config.json" file. 